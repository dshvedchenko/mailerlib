import { Client } from 'node-rest-client'



exports.Mailer = function(endpoint) {
    const client = new Client();
    return async function(email, subject, text) {
        const args = {
            data: {
                email,
                subject,
                text
            },
            headers: { "Content-Type": "application/json" }
        };

        const request = await client.post(`${endpoint}/send`, args, async(data, response) => {
            global.console.log(JSON.stringify(data));
            return data;
        })

        return request;
    }
}